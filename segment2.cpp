#include <iostream>
#include <array>
#include <fstream>
#include <time.h>


void pocetnoUbacivanje(std::array<char, 40000> &polje){
    std::ifstream ulaznaDatoteka("ulazniPodaci.txt");
    if(ulaznaDatoteka.is_open()){
        char pos = ' ';
        int i = 0;
        while(ulaznaDatoteka >> pos){
            polje[i] = pos;
            i = i + 1;
        }
        ulaznaDatoteka.close();
    }else{
        std::cout << "Datoteka se nije otvorila!" << std::endl;
    }
}

void ubaciNaPocetak(std::array<char, 40000> &polje, int velicina){
    std::ifstream ulaznaDatoteka("ulazniPodaci.txt");
    if(ulaznaDatoteka.is_open()){
        char pos = ' ';
        while(ulaznaDatoteka >> pos){
            for(int i = velicina; i > 0; i--){
                polje[i] = polje[i-1];
            }
            polje[0] = pos;
            velicina = velicina + 1;
        }
        ulaznaDatoteka.close();
    }else{
        std::cout << "Datoteka se nije otovrila!" << std::endl;
    }
}

void ubaciUSredinu(std::array<char, 40000> &polje, int velicina){
    std::ifstream ulaznaDatoteka("ulazniPodaci.txt");
    if(ulaznaDatoteka.is_open()){
        char pos = ' ';
        int n = velicina/2;
        while(ulaznaDatoteka >> pos){
            for(int i = velicina; i > n; i--){
                polje[i] = polje[i-1];
            }
            polje[n] = pos;
            velicina = velicina + 1;
        }
        ulaznaDatoteka.close();
    }else{
        std::cout << "Datoteka se nije otvorila!" << std::endl;
    }
}

void ubaciNaKraj(std::array<char, 40000> &polje, int velicina){
    std::ifstream ulaznaDatoteka("ulazniPodaci.txt");
    if(ulaznaDatoteka.is_open()){
        char pos = ' ';
        while(ulaznaDatoteka >> pos){
            polje[velicina] = pos;
            velicina = velicina + 1;
        }
        ulaznaDatoteka.close();
    }else{
        std::cout << "Datoteka se nije otvorila!" << std::endl;
    }
}

void obrisiSaPocetka(std::array<char, 40000> &polje, int velicina, int n){
    while(n > 0){
        int j = 0;
        int i;
        for(i = 1; i < velicina - j; i++){
            polje[i-1] = polje[i];
        }
        polje[i-1] = ' ';
        n = n - 1;
        j = j + 1;
    }
}

void obrisiSaSredine(std::array<char, 40000> &polje, int velicina, int n){
    int mid = velicina/2;
    while(n > 0){
        int j = 0;
        int i;
        for(i = mid+1; i < velicina - j; i++){
            polje[i-1] = polje[i];
        }
        polje[i-1] = ' ';
        n = n - 1;
        j = j + 1;
    }
}

void obrisiSaKraja(std::array<char, 40000> &polje, int velicina, int n){
    for(int i = velicina - n - 1; i < velicina; i++){
        polje[i] = ' ';
    }
}

int main(){
    std::array<char, 40000> polje;

    //ubacivanje 10000 početnih elemenata u polje
    pocetnoUbacivanje(polje);
     
    //ubacivanje 10000 elemenata na početak polja
    clock_t tStart = clock();
    ubaciNaPocetak(polje,10000);
    std::cout << "Vrijeme ubacivanja na pocetak polja: " << (double)(clock() - tStart)/CLOCKS_PER_SEC << std::endl;

    //ubacivanje 10000 elemenata u sredinu polja
    tStart = clock();
    ubaciUSredinu(polje,20000);
    std::cout << "Vrijeme ubacivanja u sredinu polja: " << (double)(clock() - tStart)/CLOCKS_PER_SEC << std::endl;

    //ubacivanje 10000 elemenata na kraj polja
    tStart = clock();
    ubaciNaKraj(polje,30000);
    std::cout << "Vrijeme ubacivanja na kraj polja: " << (double)(clock() - tStart)/CLOCKS_PER_SEC << std::endl;

    //dohvacanje elementa na indexu 1000
    tStart = clock();
    polje[1000];
    std::cout << "Vrijeme dohvacanja elementa na indexu: " << (double)(clock() - tStart)/CLOCKS_PER_SEC << std::endl;

    //brisanje 1000 elemenata sa pocetka
    tStart = clock();
    obrisiSaPocetka(polje,40000,1000);
    std::cout << "Vrijeme brisanja elemenata sa pocetka: " << (double)(clock() - tStart)/CLOCKS_PER_SEC << std::endl;

    //brisanje 1000 elemenata sa sredine
    tStart = clock();
    obrisiSaSredine(polje,39000,1000);
    std::cout << "Vrijeme brisanja elemenata sa sredine: " << (double)(clock() - tStart)/CLOCKS_PER_SEC << std::endl;

    //brisanje 1000 elemenata sa kraja
    tStart = clock();
    obrisiSaKraja(polje,38000,1000);
    std::cout << "Vrijeme brisanja elemenata sa kraja: " << (double)(clock() - tStart)/CLOCKS_PER_SEC << std::endl;
}