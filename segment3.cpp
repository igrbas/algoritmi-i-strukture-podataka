#include <iostream>
#include <forward_list>
#include <fstream>
#include <time.h>

void pocetnoUbacivanjeLista(std::forward_list<char> &lista){
    std::ifstream ulaznaDatoteka("ulazniPodaci.txt");
    if(ulaznaDatoteka.is_open()){
        char pos = ' ';
        while(ulaznaDatoteka >> pos){
            lista.push_front(pos);
        }
        ulaznaDatoteka.close();
    }else{
        std::cout << "Datoteka se nije otvorila!" << std::endl;
    }
}

void ubaciNaPocetakListe(std::forward_list<char> &lista){
    std::ifstream ulaznaDatoteka("ulazniPodaci.txt");
    if(ulaznaDatoteka.is_open()){
        char pos = ' ';
        while(ulaznaDatoteka >> pos){
            lista.push_front(pos);
        }
        ulaznaDatoteka.close();
    }else{
        std::cout << "Datoteka se nije otvorila!" << std::endl;
    }
}

void ubaciUSredinuListe(std::forward_list<char> &lista, int velicina){
    std::ifstream ulaznaDatoteka("ulazniPodaci.txt");
    if(ulaznaDatoteka.is_open()){
        char pos = ' ';
        std::forward_list<char> novaLista;
        int n = velicina/2;
        for(int i = 0; i < n; i++){
            novaLista.push_front(lista.front());
            lista.pop_front();
        }
        while(ulaznaDatoteka >> pos){
            lista.push_front(pos);
        }

        while(!novaLista.empty()){
            lista.push_front(novaLista.front());
            novaLista.pop_front();
        }
        ulaznaDatoteka.close();
    }else{
        std::cout << "Datoteka se nije otvorila!" << std::endl;
    }
}

void ubaciNaKrajListe(std::forward_list<char> &lista){
    std::ifstream ulaznaDatoteka("ulazniPodaci.txt");
    if(ulaznaDatoteka.is_open()){
        char pos = ' ';
        std::forward_list<char> novaLista;
        while(!lista.empty()){
            novaLista.push_front(lista.front());
            lista.pop_front();
        }
        while(ulaznaDatoteka >> pos){
            lista.push_front(pos);
        }
        while(!novaLista.empty()){
            lista.push_front(novaLista.front());
            novaLista.pop_front();
        }
        ulaznaDatoteka.close();
    }else{
        std::cout << "Datoteka se nije otvorila!" << std::endl;
    }
}

void obrisiSaPocetkaListe(std::forward_list<char> &lista,int n){
    for(int i = 0; i < n; i++){
        lista.pop_front();
    }
}

void obrisiSaSredineListe(std::forward_list<char> &lista, int velicina, int n){
    int mid = velicina/2;
    std::forward_list<char> novaLista;
    for(int i = 0; i < mid; i++){   
        novaLista.push_front(lista.front());
        lista.pop_front();
    }
    for(int i = 0; i < n; i++){
        lista.pop_front();
    }
    while(!novaLista.empty()){
        lista.push_front(novaLista.front());
        novaLista.pop_front();
    }
}

void obrisiSaKrajaListe(std::forward_list<char> &lista, int n){
    std::forward_list<char> novaLista;
    while(!lista.empty()){
        novaLista.push_front(lista.front());
        lista.pop_front();
    }
    for(int i = 0; i < n; i++){
        novaLista.pop_front();
    }
    while(!novaLista.empty()){
        lista.push_front(novaLista.front());
        novaLista.pop_front();
    }
}


int main(){
    std::forward_list<char> lista;

    //ubacivanje 10000 početnih elemenata u listu
    pocetnoUbacivanjeLista(lista);
    
    //ubacivanje 10000 elemenata na početak liste
    clock_t tStart = clock();
    ubaciNaPocetakListe(lista);
    std::cout << "Vrijeme ubacivanja elemenata na pocetak liste: " << (double)(clock() - tStart)/CLOCKS_PER_SEC << std::endl;

    //ubacivanje 10000 elemenata u sredinu liste
    tStart = clock();
    ubaciUSredinuListe(lista,20000);
    std::cout << "Vrijeme ubacivanja elemenata u sredinu liste: " << (double)(clock() - tStart)/CLOCKS_PER_SEC << std::endl;

    //ubacivanje 10000 elemenata na kraj liste
    tStart = clock();
    ubaciNaKrajListe(lista);
    std::cout << "Vrijeme ubacivanja elemenata na kraj liste: " << (double)(clock() - tStart)/CLOCKS_PER_SEC << std::endl;

    //dohvacanje 1000. elementa
    tStart = clock();
    auto iterator = lista.begin();
    std::advance(iterator, 1000);
    int a = *iterator;
    std::cout << "Vrijeme dohvacanja elementa: " << (double)(clock() - tStart)/CLOCKS_PER_SEC << std::endl;

    //brisanje 1000 elemenata sa pocetka liste
    tStart = clock();
    obrisiSaPocetkaListe(lista,1000);
    std::cout << "Vrijeme brisanja elemenata sa pocetka liste: " << (double)(clock() - tStart)/CLOCKS_PER_SEC << std::endl;

    //brisanje 1000 elemenata sa sredine liste
    tStart = clock();
    obrisiSaSredineListe(lista,39000,1000);
    std::cout << "Vrijeme brisanja elemenata sa sredine liste: " << (double)(clock() - tStart)/CLOCKS_PER_SEC << std::endl;

    //brisanje 1000 elemenata sa kraja liste
    tStart = clock();
    obrisiSaKrajaListe(lista,1000);
    std::cout << "Vrijeme brisanja elemenata sa kraja liste: " << (double)(clock() - tStart)/CLOCKS_PER_SEC << std::endl;
}



