# Algoritmi i strukture podataka  
<b>Segment 2</b>  
Vrijeme ubacivanja 10000 elemenata na početak polja: 1638ms  
Vrijeme ubacivanja 10000 elemenata u sredinu polja: 1084ms  
Vrijeme ubacivanja 10000 elemenata na kraj polja : 1ms  
Vrijeme dohvaćanja elementa na tisućitom indexu: 0ms  
Vrijeme brisanja 1000 elemenata sa početka polja: 109ms  
Vrijeme brisanja 1000 elemenata sa sredine polja: 55ms  
Vrijeme brisanja 1000 elemenata sa kraja polja: 0ms  

<b>Segment 3</b>  
Vrijeme ubacivanja 10000 elemenata na početak forward_liste: 2ms  
Vrijeme ubacivanja 10000 elemenata u sredinu forward_liste: 3ms  
Vrijeme ubacivanja 10000 elemenata na kraj forward_liste: 5ms  
Vrijeme dohvaćanja tisućutog elementa: 0ms  
Vrijeme brisanja 1000 elemenata sa početka forward_liste: 0ms  
Vrijeme brisanja 1000 elemenata sa sredine forward_liste: 2ms  
Vrijeme brisanja 1000 elemenata sa kraj forward_liste: 4ms  

<b>Segment 4</b>  

Sturktura 1 je bolja kod ubacivanja elemenata na kraj polja i kod brisanja elemenata sa kraja polja. Zato što samo krene sa kraja i nastavi dalje ubacivati elemente (ili brisati). Lošija je kod ubacivanja i brisanja elemenata sa sredine i početka, zato što kada ubaci jedan element na početak, sve ostale elemente mora pomaknuti u desno.Kod brisanja, za svaki obrisani element sve ostale elemente mora pomaknuti u lijevo.  

Struktura 2 je dobra kod ubacivanja i brisanja elemenata sa početka polja, zato što kod ubacivanja novog elementa treba samo postaviti pokazivać od prvog čvora na novi element(čvor). A kod brisanja pokazivać se makne. Kod ubacivanja na sredinu je bolja od array-a, ali se mora napraviti dodatna lista, jer u forward listu se mogu samo ubacivati i brisati elementi sa početka.  

Kod pristupa elementima, sturkutre su jednake.  

Time complexity strukture 1  
Ubacivanje elemenata ili brisanje elementa na kraju - O(n) = 1 - rezultati odgovaraju.  
Ubacivanje ili brisanje elementa sa početka ili sredine - O(n) = n - rezultati odgovaraju, pošto ja ubacujem 10000 elemenata pa je onda O(n) = broj_elemenata_u_polju * broj_elemenata_koji_se_ubacuju.  
Pristup elementu - O(n) = 1 - rezultati odgovaraju.  

Time complexity strukture 2  
Ubacivanje ili brisanje elementa na početku forward liste - O(n) = 1 - rezultati odgovaraju.  

<b>Segment 5</b>  
Ovaj algoritam je idealan kada su u datoteci svi podaci koje sažimamo jednaki.  
Ovaj algoritam je loš izbor kada u datoteci nema puno podataka koji se ponavljaju jedan iza drugog. Tada se može desiti da ovaj algoritam neće sažeti datoteku, već će ju proširit.  
U usporedbi sa Huffmanovim algoritmom. Ovaj algoritam je puno bolji u najboljem slučaju, a u najgorem slučaju, Huffmanov algoritam je bolji jer on zapravo može sažeti datoteku. Također RLE ima bolji time complexity O(n) = n, dok je Huffmanov time complexity O(n) = nlogn.
