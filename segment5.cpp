#include <iostream>
#include <vector>
#include <fstream>
#include <string>

std::string sazimanje(std::string &podaci){
    int count = 1;
    std::string sazetiPodaci;
    int i;
    for(i = 0; i < podaci.size()-1; i++){
        if(podaci[i] == podaci[i+1]){
            count = count + 1;
        }else{
            sazetiPodaci = sazetiPodaci + podaci[i] + std::to_string(count);
            count = 1;
        }
    }
    sazetiPodaci = sazetiPodaci + podaci[i] + std::to_string(count);
    return sazetiPodaci;
}

std::string raspakiranje(std::string &sazetiPodaci){
    std::string raspakirano;
    for(int i = 0; i < sazetiPodaci.size(); i++){
        std::string broj = "";
        int j = i + 1;
        while(isdigit(sazetiPodaci[j])){
            broj = broj + sazetiPodaci[j];
            j = j + 1;
        }
        int n = std::stoi(broj);
        for(int k = 0; k < n; k++){
            raspakirano.push_back(sazetiPodaci[i]);
        }
        i = j - 1;
    }
    return raspakirano;
}

int main(){
    std::string podaci;
    //std::ifstream ulaznaDatoteka("najboljiPodaci.txt");
    std::ifstream ulaznaDatoteka("srednjiPodaci.txt");
    //std::ifstream ulaznaDatoteka("najgoriPodaci.txt");
    if(ulaznaDatoteka.is_open()){
        char pos = ' '; 
        while(ulaznaDatoteka >> pos){
            podaci.push_back(pos);
        }
        ulaznaDatoteka.close();
    }else{
        std::cout << "Datoteka se nije otovrila!" << std::endl;
    }

    std::string sazetiPodaci = sazimanje(podaci);
    std::cout << "Velicina pocetnih podataka: " << podaci.size() <<std::endl;
    std::cout << "Velicina sazetih podataka: " << sazetiPodaci.size()<<std::endl;
    std::cout << "Velicina sazetih podataka je: " << (double)sazetiPodaci.size()/(double)podaci.size() * 100 << "% u odnosu na pocetne podatke!" << std::endl;
    std::string raspakirano = raspakiranje(sazetiPodaci);
    if(raspakirano == podaci){
        std::cout << "Sazimanje i raspakiranje je uspjelo!" << std::endl;
    }
    //std::cout << sazetiPodaci << std::endl;
    //std::cout << raspakirano << std::endl;
}